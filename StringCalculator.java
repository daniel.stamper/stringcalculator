import java.util.Arrays;

public class StringCalculator {
    /**
     * The Add() method takes an input string and returns the sum of numbers in it.
     * @param numbers a string containing a series of numbers separated by either a comma or a newline character.
     * Assumes that the input string format either starts with a number or a custom delimeter tag "//?\n"
     * Assumes that all characters in the numbers string are in the following set:
     * {"[0-9]", "-", ",", "\n"} or are a custom defined character
     * @return sum of the numbers in a string, or -1 if issue, or throws Exception for negative numbers.
     */
    public int Add(String numbers) throws Exception {
        /*
         *  The delimeter is the separator character between numbers in the input string
         * This character can be changed by adding the prefix "//?\n" to the numbers string.
         * Where the "?" is a custom delimeter character that will replace the ",".
        */
        String delimeter = ",|\n";
        int sum = 0;
        int item;

        // check for an empty numbers string
        if (numbers.length() != 0) {

            // Check the start of the string for the custom delimeter marker.
            // The regex expression is split into two components "(a)(b)" for readability.
            if (numbers.matches("(//.\n)(.*)")) {

                // set a custom delimeter
                delimeter = numbers.substring(2,3)+"|\n";
                //System.out.println("del: "+delimeter);
                // Rest of numbers after first newline
                numbers = numbers.substring(4);
                // check for an empty numbers string
                if (numbers.length() == 0) {
                    return 0;
                }
            }
            // create an array of strings by splitting the numbers string by "," or "\n"
            String[] arrayOfNumbers = numbers.split(delimeter);

            //array to store negative numbers
            int[] negatives;
            int negativesIndex = 0;
            // create an array assuming the maximum number of negative integers.
            negatives = new int[arrayOfNumbers.length];

            for (int i=0; i<arrayOfNumbers.length; i++) {
                try {
                    item = Integer.parseInt(arrayOfNumbers[i]);

                    if (item < 0) {
                        // store a list of negatives
                        negatives[negativesIndex] = item;
                        negativesIndex++;

                    } else if (item < 1001){
                        // ignore numbers larger than 1000
                        sum += item;
                    }
                } catch (NumberFormatException e) {
                    System.out.println("\nThere was an issue parsing the numbers string: "+e+"\n");
                    return -1;
                }
            }

            if (negativesIndex > 0) {
                // create a string using the negatives array (remove the default zeros)
                String negativeString = Arrays.toString(Arrays.copyOfRange(negatives, 0, negativesIndex));
                throw new Exception("Negatives not allowed. \nList of offending numbers: " + negativeString);
            }
        }

        return sum;
    }

    // Regression Tests
    public static void main(String args[]) {
        // tests
        StringCalculator strCalc= new StringCalculator();

        // Empty string test
        String nums = "";
        int sum;

        try {
            sum = strCalc.Add(nums);
            if (sum != 0) {
                System.out.println("Add() method for an empty string should return 0, but returned "+sum+" instead.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // Simple addition test.
        nums = "1,2,7";
        try {
            sum = strCalc.Add(nums);
            if (sum != 10) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The correct sum should be 10");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // floating point test.
        // java.langNumberFormatException
        nums = "1,2.9,4";
        try {
            sum = strCalc.Add(nums);
            if (sum != -1) {
                System.out.println("A string with a floating point should have thrown NumberFormatException and returned -1.");
            }

        } catch (Exception e) {
            System.out.println(e);
        }


        // negative numbers test.
        nums = "91,-2,3";
        try {
            sum = strCalc.Add(nums);
            if (sum != 92) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The correct sum should be 92");
        }
        } catch (Exception e) {
            System.out.println(e);
        }


        // multiple zeros test.
        nums = "0,000,000,1,003";
        try {
            sum = strCalc.Add(nums);
            if (sum != 4) {
                System.out.println("Add() method returned incorrect sum of "+sum+" for the following input: "+nums);
                System.out.println("The correct sum should be 4");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // random chars test.
        // java.langNumberFormatException
        nums = "a,2,3,1#";
        try {
            sum = strCalc.Add(nums);
            if (sum != -1) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("A string with a random character point should have thrown NumberFormatException and returned -1.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // Single newline simple addition test.
        nums = "1\n2,4";
        try {
            sum = strCalc.Add(nums);
            if (sum != 7) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The correct sum should be 10");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // later newline addition test.
        nums = "1,2\n7";
        try {
            sum = strCalc.Add(nums);
            if (sum != 10) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The correct sum should be 10");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // newline separator addition test.
        nums = "1\n2\n7";
        try {
            sum = strCalc.Add(nums);
            if (sum != 10) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The correct sum should be 10");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // leading separator addition test. Should return -1.
        nums = "\n1,2,7";
        try {
            sum = strCalc.Add(nums);
            if (sum != -1) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The parseInt method should have thrown an exception, and the Add() method should have returned -1.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // leading separator addition test.
        nums = ",1,2,7";
        try {
            sum = strCalc.Add(nums);
            if (sum != -1) {
                System.out.println("Add() method returned incorrect sum of " + sum);
                System.out.println("The parseInt method should have thrown an exception, and the Add() method should have returned -1.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // Part 3: Tests -------------------------------------------------------------
        // custom delimeter followed by an empty string
        nums = "//a\n";
        try {
            sum = strCalc.Add(nums);
            if (sum != 0) {
                System.out.println("Custom delimeter test 1.  where the numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 0.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // custom delimeter test 2
        nums = "//a\n23a23";
        try {
            sum = strCalc.Add(nums);
            if (sum != 46) {
                System.out.println("Custom delimeter test 2.  where the numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 46.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // unescaped special char custom delimeter should fail
        nums = "//\n23a23a23";
        try {
            sum = strCalc.Add(nums);
            if (sum != -1) {
                System.out.println("Custom delimeter test 3: Unescaped special char. where the numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 46.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // I'm not entirely sure why this test is failing.
        // custom delimeter test 4
        nums = "//$\n1$2$3";
        try {
            sum = strCalc.Add(nums);
            if (sum != 6) {
                System.out.println("Custom delimeter test 4: The numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 6.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }


        // custom delimeter test 5
        nums = "//@\n2@3@8";
        try {
            sum = strCalc.Add(nums);
            if (sum != 13) {
                System.out.println("Custom delimeter test 5: The numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 13.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        // Bonus 1 test --------------------------------------------
        // custom delimeter test 5
        nums = "//@\n2@3000@1000";
        try {
            sum = strCalc.Add(nums);
            if (sum != 1002) {
                System.out.println("Bonus test 1: The numbers string is: "+nums);
                System.out.println("Add() method returned incorrect sum of " + sum + "should have returned 1002.");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
